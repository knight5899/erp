from .models import Warehouse, Inventory, Flow, CountingList, Requisition
from rest_framework.exceptions import APIException
from rest_framework import serializers
from django.db.models import Sum, F


class WarehouseSerializer(serializers.ModelSerializer):
    goods_total = serializers.SerializerMethodField('get_goods_total')
    manager_name = serializers.CharField(source='manager.name', default=None, read_only=True, label='管理员名称')

    class Meta:
        model = Warehouse
        read_only_fields = ['id', 'goods_total', 'manager_name']
        fields = ['name', 'number', 'manager', 'phone', 'address', 'status', 'order',
                  'remark', *read_only_fields]

    def get_goods_total(self, obj):
        return obj.inventories.filter(goods__status=True).aggregate(
            goods_total=Sum('quantity')).get('goods_total')


class InventorySerializer(serializers.ModelSerializer):
    goods = serializers.SerializerMethodField('get_goods')
    warehouse_name = serializers.SerializerMethodField('get_warehouse_name')
    total = serializers.SerializerMethodField('get_total')

    class Meta:
        model = Inventory
        fields = ['goods', 'quantity', 'warehouse_name', 'total']
        read_only_fields = fields

    def get_goods(self, obj):
        return {
            'code': obj.goods.code,
            'name': obj.goods.name,
            'spec': obj.goods.spec,
            'unit': obj.goods.unit,
            'category_name': obj.goods.category.name if obj.goods.category else '',
            'purchase_price': obj.goods.purchase_price,
        }

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_total(self, obj):
        return self.context['request'].user.teams.inventories.all().aggregate(
            quantity=Sum('quantity'), amount=Sum(F('quantity') * F('goods__purchase_price')))


class FlowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flow
        fields = ['create_datetime', 'goods_code', 'goods_name', 'spec', 'unit',
                  'warehouse_name', 'type', 'change_quantity', 'remain_quantity', 'operator',
                  'requisition', 'counting_list', 'purchase_order', 'sales_order']
        read_only_fields = fields


class CountingListSerializer(serializers.ModelSerializer):
    goods_set = serializers.SerializerMethodField('get_goods_set')

    class Meta:
        model = CountingList
        read_only_fields = ['id', 'warehouse_name', 'goods_set', 'date']
        fields = ['warehouse', 'remark', *read_only_fields]

    def validate(self, data):
        if not data.get('warehouse'):
            raise serializers.ValidationError

        # goods_set
        goods_set = self.context['request'].data.get('goods_set', [])
        if not goods_set:
            raise APIException({'message': '没有添加商品'})

        for item in goods_set:
            if item.get('id') is None or not item.get('quantity') or item['quantity'] <= 0:
                raise APIException({'message': '商品数量错误'})

        return data

    def get_goods_set(self, obj):
        return obj.goods_set.all().values('id', 'code', 'name', 'spec', 'unit', 'quantity',
                                          'before_counting', 'purchase_price')


class RequisitionSerializer(serializers.ModelSerializer):
    goods_set = serializers.SerializerMethodField('get_goods_set')

    class Meta:
        model = Requisition
        read_only_fields = ['id', 'out_warehouse_name', 'into_warehouse_name', 'goods_set']
        fields = ['out_warehouse', 'into_warehouse', 'date', 'remark', *read_only_fields]

    def validate(self, data):
        if not data.get('out_warehouse') or not data.get('into_warehouse') or not data.get('date'):
            raise serializers.ValidationError

        # goods_set
        goods_set = self.context['request'].data.get('goods_set', [])
        if not goods_set:
            raise APIException({'message': '没有添加商品'})

        for item in goods_set:
            if item.get('id') is None or not item.get('quantity') or item['quantity'] <= 0:
                raise APIException({'message': '商品数量错误'})

        return data

    def get_goods_set(self, obj):
        return obj.goods_set.all().values('id', 'code', 'name', 'spec', 'unit', 'quantity')
