from rest_framework.routers import SimpleRouter
from django.urls import path
from . import views


router = SimpleRouter()
router.register('roles', views.RoleViewSet, 'role')
router.register('subusers', views.SubusertViewSet, 'subuer')

urlpatterns = [
    path('accounts/', views.AccountViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('accounts/<str:pk>/', views.AccountViewSet.as_view({'put': 'update', 'delete': 'destroy'})),
    path('sellers/', views.SellerViewSet.as_view({'get': 'list'})),
    path('bookkeeping/', views.BookkeepingViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('bookkeeping/<str:pk>/', views.BookkeepingViewSet.as_view({'delete': 'destroy'})),
    path('statistical_account/', views.StatisticalAccountViewSet.as_view({'get': 'list'})),
    path('users/', views.UserViewSet.as_view({'get': 'list'})),
    *router.urls,
]
