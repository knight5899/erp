from account.serializers import RoleSerializer,  AccountSerializer, BookkeepingSerializer
from account.serializers import SubuserSerializer, SubuserCreateSerializer
from account.permissions import RolePermission, SubuserPermission
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import no_body, swagger_auto_schema
from rest_framework.exceptions import ValidationError
from django.contrib.auth.hashers import make_password
from account.paginations import BookkeepingPagination
from account.schemas import reset_password_request
from django.db.models.functions import Coalesce
from utils.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets, status
from django.db.models import Sum, Value
from account.models import Account
from account.models import Role
from user.models import User
import pendulum


class RoleViewSet(viewsets.ModelViewSet):
    """角色"""

    serializer_class = RoleSerializer
    permission_classes = [IsAuthenticated, RolePermission]

    def get_queryset(self):
        return Role.objects.filter(teams=self.request.user.teams)


class SubusertViewSet(viewsets.ModelViewSet):
    """子用户"""

    serializer_class = SubuserSerializer
    permission_classes = [IsAuthenticated, SubuserPermission]

    def get_queryset(self):
        return User.objects.filter(teams=self.request.user.teams)

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method == 'POST':
            return SubuserCreateSerializer
        else:
            return SubuserSerializer

    def create(self, request, *args, **kwargs):
        username = request.data.get('username')

        if User.objects.filter(username=username).exists():
            raise ValidationError({'message': '用户名已存在'})
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        username = request.data.get('username')

        if User.objects.filter(username=username).exclude(id=instance.id).exists():
            raise ValidationError({'message': '用户名已存在'})

        return super().update(request, *args, **kwargs)

    def perform_destroy(self, instance):
        if instance.is_manager:
            raise ValidationError({'message': '超级账号无法删除'})
        return super().perform_destroy(instance)

    @swagger_auto_schema(request_body=reset_password_request, responses={200: ''})
    @action(detail=True, methods=['post'])
    def reset_password(self, request, *args, **kwargs):
        """重置密码"""
        try:
            password = request.data['password']
        except KeyError as missing_field:
            raise ValidationError({'message': f'缺少{missing_field}字段'})

        instance = self.get_object()
        instance.password = make_password(password)
        instance.save()

        return Response(status=status.HTTP_200_OK)


class AccountViewSet(viewsets.ModelViewSet):
    """结算账户"""

    serializer_class = AccountSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [OrderingFilter, SearchFilter, DjangoFilterBackend]
    search_fields = ['name', 'number']
    ordering_fields = ['id', 'name', 'number', 'order']
    ordering = ['order', 'id']

    def get_queryset(self):
        return Account.objects.filter(teams=self.request.user.teams)


class SellerViewSet(viewsets.ModelViewSet):
    """list"""
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = User.objects.filter(teams=request.user.teams)

        roles = request.user.roles.all()
        if not roles:  # 没有设置角色默认拥有全部权限
            return Response(queryset.values('id', 'username', 'name'))

        for role in roles:
            if 'CHANGE_SELLER' in role.permissions:
                return Response(queryset.values('id', 'username', 'name'))

        return Response([{'id': request.user.id, 'username': request.user.username, 'name': request.user.name}])


class BookkeepingViewSet(viewsets.ModelViewSet):
    """list, create, destroy"""
    serializer_class = BookkeepingSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = BookkeepingPagination

    def get_queryset(self):
        return self.request.user.teams.bookkeeping_set.all().order_by('-create_datetime')

    def perform_create(self, serializer):
        serializer.save(teams=self.request.user.teams, recorder=self.request.user)


class StatisticalAccountViewSet(viewsets.ModelViewSet):
    """list"""
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        accoutns = self.request.query_params.get('accounts')
        accoutns = accoutns.split(',') if accoutns else []
        start_date = self.request.GET.get('start_date')
        end_date = self.request.GET.get('end_date')
        end_date = pendulum.parse(end_date).add(days=1)

        bookkeeping_queryset = self.request.user.teams.bookkeeping_set.filter(
            account_id__in=accoutns)
        purchase_queryset = self.request.user.teams.purchase_order_set.filter(
            account_id__in=accoutns, is_return=False)
        sales_queryset = self.request.user.teams.sales_order_set.filter(
            account_id__in=accoutns, is_return=False)

        if start_date:
            bookkeeping_queryset = bookkeeping_queryset.filter(create_datetime__gte=start_date)
            purchase_queryset = purchase_queryset.filter(date__gte=start_date)
            sales_queryset = sales_queryset.filter(date__gte=start_date)

        if end_date:
            bookkeeping_queryset = bookkeeping_queryset.filter(create_datetime__lte=end_date)
            purchase_queryset = purchase_queryset.filter(date__lte=end_date)
            sales_queryset = sales_queryset.filter(date__lte=end_date)

        return bookkeeping_queryset, purchase_queryset, sales_queryset

    def list(self, request, *args, **kwargs):
        bookkeeping_queryset, purchase_queryset, sales_queryset = self.get_queryset()

        amount_list = bookkeeping_queryset.values_list('amount', flat=True)
        expenditure = purchase_queryset.aggregate(amount=Coalesce(Sum('amount'), Value(0)))['amount']
        revenue = sales_queryset.aggregate(amount=Coalesce(Sum('amount'), Value(0)))['amount']

        for amount in amount_list:
            if amount > 0:
                revenue += amount
            else:
                expenditure -= amount

        return Response({'revenue': revenue, 'expenditure': expenditure})


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        results = User.objects.filter(teams=request.user.teams).values('id', 'username', 'name')
        return Response(data=results)
