from django.contrib.auth.hashers import make_password
from rest_framework.exceptions import APIException
from .models import Role, Account, Bookkeeping
from rest_framework import serializers
from user.models import User


class RoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        read_only_fields = ['id']
        fields = ['name', 'remark', 'permissions', *read_only_fields]

    def create(self, validated_data):
        teams = self.context['request'].user.teams
        validated_data['teams'] = teams
        return super().create(validated_data)


class SubuserCreateSerializer(serializers.ModelSerializer):
    role_names = serializers.SerializerMethodField('get_role_names')

    class Meta:
        model = User
        read_only_fields = ['id', 'create_date', 'role_names']
        fields = ['username', 'password', 'name', 'phone', 'email', 'sex', 'status',
                  'roles', *read_only_fields]
        extra_kwargs = {'password': {'write_only': True}}

    def validate_roles(self, instances):
        teams = self.context['request'].user.teams
        if instances:
            instance_ids = [instance.id for instance in instances]
            instances = Role.objects.filter(id__in=instance_ids, teams=teams)

            if len(instance_ids) != len(instances):
                raise APIException({'message': '角色不存在'})
        return instances

    def create(self, validated_data):
        teams = self.context['request'].user.teams

        # 设置密码
        validated_data['teams'] = teams
        validated_data['password'] = make_password(validated_data['password'])
        return super().create(validated_data)

    def get_role_names(self, obj):
        return obj.roles.all().values_list('name', flat=True)


class SubuserSerializer(serializers.ModelSerializer):
    role_names = serializers.SerializerMethodField('get_role_names')

    class Meta:
        model = User
        read_only_fields = ['id', 'create_date', 'role_names']
        fields = ['username', 'name', 'phone', 'email', 'sex', 'status', 'roles', *read_only_fields]

    def validate_roles(self, instances):
        teams = self.context['request'].user.teams
        if instances:
            instance_ids = [instance.id for instance in instances]
            instances = Role.objects.filter(id__in=instance_ids, teams=teams)

            if len(instance_ids) != len(instances):
                raise APIException({'message': '角色不存在'})
        return instances

    def get_role_names(self, obj):
        return obj.roles.all().values_list('name', flat=True)


class AccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        read_only_fields = ['id']
        fields = ['name', 'number', 'holder', 'type', 'status', 'order', 'remark', *read_only_fields]

    def create(self, validated_data):
        teams = self.context['request'].user.teams
        validated_data['teams'] = teams
        return super().create(validated_data)


class BookkeepingSerializer(serializers.ModelSerializer):
    account_name = serializers.SerializerMethodField('get_account_name')

    class Meta:
        model = Bookkeeping
        fields = ['id', 'create_datetime', 'account', 'account_name', 'amount',
                  'recorder', 'remark']
        read_only_fields = ['id', 'create_datetime', 'account_name', 'recorder']

    def get_account_name(self, obj):
        return obj.account.name
