from drf_yasg.openapi import Schema, TYPE_OBJECT, TYPE_STRING


reset_password_request = Schema(type=TYPE_OBJECT, required=['password'], properties={
    'password': Schema(type=TYPE_STRING, title='编号')
})
