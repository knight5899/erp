from rest_framework.decorators import api_view
from rest_framework import status, exceptions
from rest_framework.response import Response
from .models import Teams
from warehouse.models import Inventory
from rest_framework import viewsets
from django.contrib import auth
from django.db.models import F
from .serializers import ConfigSerializer
from utils.permissions import IsAuthenticated

from django.contrib.auth.hashers import make_password, check_password


@api_view(['POST'])
def login(request):
    username = request.data.get('username')
    password = request.data.get('password')

    user = auth.authenticate(username=username, password=password)
    if not user:
        raise exceptions.AuthenticationFailed({'message': '账号密码错误'})

    auth.login(request, user)
    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def logout(request):
    auth.logout(request)
    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def set_password(request):
    """设置密码: username, password"""
    try:
        username = request.data.get('username')
        old_password = request.data.get('old_password')
        new_password = request.data.get('new_password')
    except KeyError as missing_field:
        raise exceptions.ValidationError({'message': f'缺少字段[{missing_field}]'})

    user = auth.authenticate(username=username, password=old_password)
    if not user:
        raise exceptions.AuthenticationFailed({'message': '账号密码错误'})

    user.set_password(new_password)
    user.save()

    auth.logout(request)
    return Response(status=status.HTTP_200_OK)


@api_view(['GET'])
def get_info(request):
    if not request.user.is_authenticated:
        raise exceptions.AuthenticationFailed({'message': '未登录'})

    teams = request.user.teams
    inventory_warning_count = Inventory.objects.filter(
        teams=teams, quantity__lte=F('goods__inventory_warning_lower_limit')).count()
    data = {
        'id': request.user.id,
        'username': request.user.username,
        'name': request.user.name,
        'inventory_warning_count': inventory_warning_count,
    }
    return Response(data=data, status=status.HTTP_200_OK)


class ConfigViewSet(viewsets.ModelViewSet):
    """配置: retrieve, post, update"""
    serializer_class = ConfigSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user.teams
