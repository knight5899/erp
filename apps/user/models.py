from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django_mysql.models import JSONField
from django.db import models


class Teams(models.Model):
    phone = models.CharField(max_length=12, unique=True, verbose_name='手机号')
    goods_code_counting = models.CharField(max_length=8, default='0000000', verbose_name='商品条码计数')
    auto_stock_in = models.BooleanField(default=False, verbose_name='自动入库')
    auto_stock_out = models.BooleanField(default=False, verbose_name='自动出库')
    expire_date = models.DateTimeField(verbose_name='到期日期')


class UserManager(BaseUserManager):
    def create(self, username, password, **kwargs):
        user = self.model(username=username, **kwargs)
        user.set_password(password)
        user.save(self._db)
        return user


class User(AbstractBaseUser):
    SEX = [
        ('man', '男'),
        ('woman', '女'),
    ]

    username = models.CharField(max_length=24, unique=True, verbose_name='用户名')
    name = models.CharField(max_length=36, verbose_name='姓名')
    phone = models.CharField(max_length=12, verbose_name='手机号')
    email = models.CharField(max_length=256, blank=True, null=True, verbose_name='邮箱')
    sex = models.CharField(max_length=32, choices=SEX, null=True, verbose_name='性别')
    status = models.BooleanField(default=True, verbose_name='状态')
    teams = models.ForeignKey('user.Teams', models.CASCADE, related_name='users', verbose_name='组')

    roles = models.ManyToManyField('account.Role', blank=True, related_name='users', verbose_name='角色')
    is_manager = models.BooleanField(default=False, verbose_name='管理员状态')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')

    USERNAME_FIELD = 'username'
    objects = UserManager()
