from goods.views import CategoryViewSet, GoodsViewSet, GoodsImageViewSet
from rest_framework.routers import SimpleRouter


router = SimpleRouter()
router.register('categories', CategoryViewSet, 'category')
router.register('goods', GoodsViewSet, 'goods')
router.register('goods_images', GoodsImageViewSet, 'goods_image')
urlpatterns = router.urls
