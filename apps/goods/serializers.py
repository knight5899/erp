from rest_framework.exceptions import APIException
from .models import Goods, Category, GoodsImage
from warehouse.models import Inventory, Flow
from purchase.models import ChangeRecord
from warehouse.models import Warehouse
from rest_framework import serializers
from django.db import transaction


class CategorySerializer(serializers.ModelSerializer):
    goods_count = serializers.SerializerMethodField('get_goods_count')

    class Meta:
        model = Category
        read_only_fields = ['id', 'goods_count']
        fields = ['name', 'description', 'order', *read_only_fields]

    def get_goods_count(self, obj):
        return obj.goods.all().count()

    def create(self, validated_data):
        teams = self.context['request'].user.teams
        validated_data['teams'] = teams
        return super().create(validated_data)


class GoodsImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = GoodsImage
        read_only_fields = ['id', 'name']
        fields = ['file', *read_only_fields]

    def create(self, validated_data):
        teams = self.context['request'].user.teams
        validated_data['name'] = validated_data['file'].name
        validated_data['teams'] = teams
        return super().create(validated_data)


class InitInventorySerializer(serializers.ModelSerializer):
    warehouse_name = serializers.CharField(source='warehouse.name', read_only=True, label='仓库名称')
    quantity = serializers.IntegerField(required=True, label='数量')

    class Meta:
        model = Inventory
        read_only_fields = ['warehouse_name']
        fields = ['warehouse', 'quantity', *read_only_fields]


class GoodsSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category.name', read_only=True, label='分类名称')
    image_items = GoodsImageSerializer(source='images', many=True, read_only=True, label='商品图片')
    inventory_items = InitInventorySerializer(source='inventories', many=True, required=False, label='库存信息')

    class Meta:
        model = Goods
        read_only_fields = ['id', 'category_name', 'image_items']
        fields = ['name', 'code', 'number', 'category', 'unit', 'spec', 'enable_shelf_life',
                  'shelf_life_days', 'shelf_life_warning_days', 'status', 'order', 'remark',
                  'purchase_price', 'retail_price', 'description', 'inventory_warning_lower_limit',
                  'inventory_warning_upper_limit', 'images', 'inventory_items', * read_only_fields]
        extra_kwargs = {'images': {'required': False}}

    def validate_number(self, value):
        teams = self.context['request'].user.teams
        queryset = Goods.objects.filter(teams=teams, number=value)
        if self.instance:
            queryset = queryset.exclude(id=self.instance.id)

        if queryset.exists():
            raise APIException({'message': '编号已存在'})

        return value

    def validate_code(self, value):
        if value is not None:
            teams = self.context['request'].user.teams
            queryset = Goods.objects.filter(teams=teams, code=value)

            if self.instance:
                queryset = queryset.exclude(id=self.instance.id)

            if queryset.exists():
                raise APIException({'message': '条码已存在'})

        return value

    def validate_category(self, instance):
        teams = self.context['request'].user.teams
        if instance:
            instance = Category.objects.filter(id=instance.id, teams=teams).first()
            if not instance:
                raise APIException({'message': '商品分类不存在'})
        return instance

    @transaction.atomic
    def create(self, validated_data):
        request = self.context['request']
        teams = request.user.teams

        validated_data['teams'] = teams
        inventory_items = validated_data.pop('inventories', [])
        instance = super().create(validated_data)

        # 初始化仓库
        flows = []
        inventories = []
        for warehouse in Warehouse.objects.filter(teams=teams):
            for item in inventory_items:
                if item['warehouse'].id == warehouse.id:
                    quantity = item['quantity']
                    break
            else:
                quantity = 0

            inventories.append(Inventory(warehouse=warehouse, goods=instance, quantity=quantity, teams=teams))

            if quantity != 0:
                flows.append(Flow(goods=instance, goods_code=instance.number, goods_name=instance.name,
                                  type='初始化库存', spec=instance.spec, unit=instance.unit,
                                  warehouse=warehouse, warehouse_name=warehouse.name,
                                  change_quantity=quantity, remain_quantity=quantity,
                                  operator=request.user, teams=teams))
        else:
            Inventory.objects.bulk_create(inventories)
            Flow.objects.bulk_create(flows)

        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        teams = self.context['request'].user.teams
        validated_data.pop('inventories', [])

        # 创建采购价变更记录
        if instance.purchase_price != validated_data['purchase_price']:
            ChangeRecord.objects.create(goods=instance, change_method='手动',
                                        before_change=instance.purchase_price,
                                        after_change=validated_data['purchase_price'],
                                        operator=self.context['request'].user, teams=teams)

        return super().update(instance, validated_data)
