from .serializers import CategorySerializer, GoodsSerializer, GoodsImageSerializer
from utils.permissions import IsAuthenticated, PurchasePricePermission
from goods.schemas import get_number_response, get_code_response
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from .permissions import CategoryPermission, GoodsPermission
from goods.models import Goods, GoodsImage, Category
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from .paginations import GoodsPagination
from goods.filters import GoodsFilter
from rest_framework import viewsets
from rest_framework import mixins
from django.db import transaction
import pendulum


class CategoryViewSet(viewsets.ModelViewSet):
    """商品分类"""

    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated, CategoryPermission]
    filter_backends = [OrderingFilter, SearchFilter]
    search_fields = ['name']
    ordering_fields = ['id', 'name', 'order']
    ordering = ['order', '-id']

    def get_queryset(self):
        return Category.objects.filter(teams=self.request.user.teams)


class GoodsImageViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):
    """商品图片"""

    serializer_class = GoodsImageSerializer
    permission_classes = [IsAuthenticated]
    queryset = GoodsImage.objects.all()


class GoodsViewSet(viewsets.ModelViewSet):
    """商品"""

    serializer_class = GoodsSerializer
    permission_classes = [IsAuthenticated, GoodsPermission, PurchasePricePermission]
    pagination_class = GoodsPagination
    filter_backends = [SearchFilter, OrderingFilter, DjangoFilterBackend]
    filterset_class = GoodsFilter
    search_fields = ['name', 'code']
    ordering_fields = ['id', 'name', 'code', 'purchase_price', 'retail_price', 'order']
    ordering = ['order', 'id']

    def get_queryset(self):
        return Goods.objects.filter(teams=self.request.user.teams)

    @transaction.atomic
    def perform_create(self, serializer):
        instance = serializer.save()
        if instance.code and instance.code.isdigit():
            teams = self.request.user.teams
            teams.goods_code_counting = instance.code
            teams.save()

    @swagger_auto_schema(responses=get_code_response)
    @action(detail=False, methods=['get'])
    def code(self, request, *args, **kwargs):
        # 获取条码

        teams = request.user.teams
        code = str(int(teams.goods_code_counting) + 1).zfill(7)
        return Response(data={'code': code})

    @swagger_auto_schema(responses=get_number_response)
    @action(detail=False, methods=['get'])
    def number(self, request, *args, **kwargs):
        # 获取编号

        teams = request.user.teams
        today = pendulum.now()
        count = Goods.objects.filter(teams=teams, create_time__year=today.year,
                                     create_time__month=today.month,
                                     create_time__day=today.day).count()

        while True:
            number = 'SP' + today.format('YYYYMMDD') + str(count + 1).zfill(3)
            if not Goods.objects.filter(number=number, teams=teams).exists():
                break
            count += 1

        return Response(data={'number': number})
