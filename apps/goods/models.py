from django.db import models


class GoodsImage(models.Model):
    """商品图片"""
    file = models.ImageField(verbose_name='文件')
    name = models.CharField(max_length=256, null=True, blank=True, verbose_name='文件名称')
    goods = models.ForeignKey('goods.Goods', models.SET_NULL, null=True, related_name='images', verbose_name='商品')
    teams = models.ForeignKey('user.Teams', models.CASCADE, related_name='goods_images')


class Goods(models.Model):
    """商品"""
    name = models.CharField(max_length=64, verbose_name='名称')
    code = models.CharField(max_length=64, null=True, blank=True, verbose_name='条形码')
    number = models.CharField(max_length=64, verbose_name='编号')
    category = models.ForeignKey('goods.Category', models.CASCADE, related_name='goods', null=True, verbose_name='分类')
    unit = models.CharField(max_length=36, null=True, blank=True, verbose_name='单位')
    spec = models.CharField(max_length=36, null=True, blank=True, verbose_name='规格')
    enable_shelf_life = models.BooleanField(default=False, verbose_name='启用保质期')
    shelf_life_days = models.IntegerField(null=True, verbose_name='保质期天数')
    shelf_life_warning_days = models.IntegerField(null=True, verbose_name='保质期预警天数')
    status = models.BooleanField(default=True, verbose_name='状态')
    order = models.IntegerField(default=100, verbose_name='排序')
    remark = models.CharField(max_length=256, null=True, blank=True, verbose_name='备注')

    purchase_price = models.FloatField(default=0, verbose_name='采购价')
    retail_price = models.FloatField(default=0, verbose_name='零售价')
    description = models.CharField(max_length=256, null=True, blank=True, verbose_name='描述')

    inventory_warning_lower_limit = models.FloatField(default=0, verbose_name='库存下限')
    inventory_warning_upper_limit = models.FloatField(default=5000, verbose_name='库存上限')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建日期')
    teams = models.ForeignKey('user.Teams', models.CASCADE, related_name='goods_set')

    class Meta:
        unique_together = [('number', 'teams')]


class Category(models.Model):
    """商品分类"""
    name = models.CharField(max_length=36, verbose_name='名称')
    description = models.CharField(max_length=256, null=True, blank=True, verbose_name='描述')
    order = models.IntegerField(default=100, verbose_name='排序')
    teams = models.ForeignKey('user.Teams', models.CASCADE, related_name='categories')
