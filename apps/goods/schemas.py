from drf_yasg.openapi import Schema, TYPE_OBJECT, TYPE_STRING


get_number_response = {
    200: Schema(type=TYPE_OBJECT, required=[''], properties={
        'number': Schema(type=TYPE_STRING, title='编号')
    }),
}

get_code_response = {
    200: Schema(type=TYPE_OBJECT, required=[''], properties={
        'code': Schema(type=TYPE_STRING, title='条码')
    }),
}
