from django_filters.rest_framework.filters import DateTimeFilter, CharFilter
from django_filters.rest_framework import FilterSet
from goods.models import Goods
from django.db.models import Q


class GoodsFilter(FilterSet):
    code_or_number = CharFilter(method='filter_code_or_number', label='精确查询')

    class Meta:
        model = Goods
        fields = ['name', 'category', 'status', 'code', 'number']

    def filter_code_or_number(self, queryset, name, value):
        return queryset.filter(Q(code=value) | Q(number=value))
