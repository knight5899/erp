from rest_framework.pagination import PageNumberPagination


class IncomeExpenditureProjectPagination(PageNumberPagination):
    page_size = 16
    page_query_param = 'page'
    max_page_size = 9999


class IncomeExpenditurePagination(PageNumberPagination):
    page_size = 16
    page_query_param = 'page'
    max_page_size = 64
