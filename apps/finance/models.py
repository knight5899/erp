from django.db import models


CATEGORY = [
    ('income', '收入'),
    ('expenditure', '支出'),
]


class IncomeExpenditureProject(models.Model):
    """收支项目"""

    type = models.CharField(max_length=64, verbose_name='项目类型')
    category = models.CharField(max_length=64, choices=CATEGORY, verbose_name='收支类别')
    teams = models.ForeignKey('user.Teams', models.CASCADE, related_name='income_expenditure_projects')


class IncomeExpenditure(models.Model):
    """日常收支"""

    number = models.CharField(max_length=64, verbose_name='编号')
    category = models.CharField(max_length=64, choices=CATEGORY, verbose_name='收支类别')
    business_time = models.DateTimeField(verbose_name='业务日期')
    client = models.ForeignKey('sales.Client', models.SET_NULL, null=True, related_name='income_expenditures', verbose_name='客户')
    supplier = models.ForeignKey('purchase.Supplier', models.SET_NULL, null=True, related_name='income_expenditures', verbose_name='供应商')
    payment_unit = models.CharField(max_length=64, blank=True, null=True, verbose_name='收款/付款单位')
    operator = models.ForeignKey('user.User', models.SET_NULL, null=True, related_name='income_expenditures', verbose_name='经手人')
    operator_username = models.CharField(max_length=64, verbose_name='经手人用户名')
    project = models.ForeignKey('finance.IncomeExpenditureProject', models.SET_NULL, null=True, related_name='income_expenditures', verbose_name='收支项目')
    project_type = models.CharField(max_length=64, verbose_name='项目类型')
    account = models.ForeignKey('account.Account', models.SET_NULL, null=True, related_name='income_expenditures', verbose_name='结算账户')
    account_name = models.CharField(max_length=64, verbose_name='结算账户名称')
    total_amount = models.DecimalField(max_digits=14, decimal_places=2, verbose_name='应收/应付金额')
    payment_amount = models.DecimalField(max_digits=14, decimal_places=2, verbose_name='实收/实付金额')
    remark = models.CharField(max_length=256, blank=True, null=True, verbose_name='备注')

    is_delete = models.BooleanField(default=False, verbose_name='删除状态')
    creator = models.ForeignKey('user.User', models.SET_NULL, null=True, related_name='created_income_expenditures', verbose_name='创建人')
    creator_username = models.CharField(max_length=64, verbose_name='创建人用户名')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建日期')
    teams = models.ForeignKey('user.Teams', models.CASCADE, related_name='income_expenditures')

    class Meta:
        unique_together = [('number', 'teams')]
