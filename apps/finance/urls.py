from finance.views import IncomeExpenditureProjectViewSet, IncomeExpenditureViewSet
from rest_framework.routers import SimpleRouter


router = SimpleRouter()
router.register('income_expenditure_projects', IncomeExpenditureProjectViewSet, 'income_expenditure_project')
router.register('income_expenditures', IncomeExpenditureViewSet, 'income_expenditure')
urlpatterns = router.urls
