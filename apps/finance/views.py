from finance.serializers import IncomeExpenditureProjectSerializer, IncomeExpenditureSerializer
from finance.paginations import IncomeExpenditureProjectPagination, IncomeExpenditurePagination
from finance.models import IncomeExpenditureProject, IncomeExpenditure
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from finance.filters import IncomeExpenditureFilter
from finance.schemas import get_number_response
from drf_yasg.utils import swagger_auto_schema
from utils.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets
import pendulum


class IncomeExpenditureProjectViewSet(viewsets.ModelViewSet):
    """收支项目"""

    serializer_class = IncomeExpenditureProjectSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = IncomeExpenditureProjectPagination
    filter_backends = [OrderingFilter, SearchFilter, DjangoFilterBackend]
    search_fields = ['type']
    ordering_fields = ['id', 'type']
    ordering = ['-id']

    def get_queryset(self):
        return IncomeExpenditureProject.objects.filter(teams=self.request.user.teams)


class IncomeExpenditureViewSet(viewsets.ModelViewSet):
    """日常收支"""

    serializer_class = IncomeExpenditureSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = IncomeExpenditurePagination
    filter_backends = [OrderingFilter, SearchFilter, DjangoFilterBackend]
    filterset_class = IncomeExpenditureFilter
    search_fields = ['number', 'remark']
    ordering_fields = ['id', 'business_time', 'total_amount', 'payment_amount', 'create_time']
    ordering = ['-id']

    def get_queryset(self):
        return IncomeExpenditure.objects.filter(is_delete=False, teams=self.request.user.teams)

    def perform_destroy(self, instance):
        instance.is_delete = True
        instance.save()

    @swagger_auto_schema(responses=get_number_response)
    @action(detail=False, methods=['get'])
    def number(self, request, *args, **kwargs):
        # 获取编号
        teams = request.user.teams
        today = pendulum.now()
        count = IncomeExpenditure.objects.filter(teams=teams, create_time__year=today.year,
                                                 create_time__month=today.month,
                                                 create_time__day=today.day).count()

        while True:
            number = 'SZ' + today.format('YYYYMMDD') + str(count + 1).zfill(3)
            if not IncomeExpenditure.objects.filter(number=number, teams=teams).exists():
                break
            count += 1

        return Response(data={'number': number})
