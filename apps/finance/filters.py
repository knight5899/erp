from django_filters.filters import DateFilter
from django_filters.rest_framework import FilterSet
from finance.models import IncomeExpenditure


class IncomeExpenditureFilter(FilterSet):
    start_date = DateFilter(field_name='business_time', lookup_expr='gte', label='开始日期')
    end_date = DateFilter(field_name='business_time', lookup_expr='lt', label='结束日期')

    class Meta:
        model = IncomeExpenditure
        fields = ['start_date', 'end_date', 'category', 'client', 'supplier', 'operator',
                  'project', 'account']
