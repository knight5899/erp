from finance.models import IncomeExpenditureProject, IncomeExpenditure
from rest_framework.exceptions import APIException
from rest_framework import serializers
from purchase.models import Supplier
from account.models import Account
from sales.models import Client
from user.models import User


class IncomeExpenditureProjectSerializer(serializers.ModelSerializer):
    category_display = serializers.CharField(source='get_category_display', read_only=True, label='收支类别')

    class Meta:
        model = IncomeExpenditureProject
        read_only_fields = ['id', 'category_display']
        fields = ['type', 'category', *read_only_fields]

    def create(self, validated_data):
        validated_data['teams'] = self.context['request'].user.teams
        return super().create(validated_data)


class IncomeExpenditureSerializer(serializers.ModelSerializer):
    category_display = serializers.CharField(source='get_category_display', read_only=True, label='收支类别')

    class Meta:
        model = IncomeExpenditure
        read_only_fields = ['id', 'payment_unit', 'operator_username', 'project_type', 'account_name',
                            'creator', 'creator_username', 'create_time', 'category_display']
        fields = ['number', 'category', 'business_time', 'client', 'supplier', 'operator', 'project',
                  'account', 'total_amount', 'payment_amount', 'remark', *read_only_fields]
        extra_kwargs = {
            'operator': {'required': True},
            'project': {'required': True},
            'account': {'required': True},
        }

    def validate_number(self, value):
        teams = self.context['request'].user.teams
        queryset = IncomeExpenditure.objects.filter(teams=teams, number=value)
        if self.instance:
            queryset = queryset.exclude(id=self.instance.id)

        if queryset.exists():
            raise APIException({'message': '编号已存在'})

        return value

    def validate_client(self, instance):
        teams = self.context['request'].user.teams
        if instance:
            instance = Client.objects.filter(id=instance.id, teams=teams).first()
            if not instance:
                raise APIException({'message': '客户不存在'})
        return instance

    def validate_supplier(self, instance):
        teams = self.context['request'].user.teams
        if instance:
            instance = Supplier.objects.filter(id=instance.id, teams=teams).first()
            if not instance:
                raise APIException({'message': '供应商不存在'})
        return instance

    def validate_operator(self, instance):
        teams = self.context['request'].user.teams
        if instance:
            instance = User.objects.filter(id=instance.id, teams=teams).first()
            if not instance:
                raise APIException({'message': '经手人不存在'})
        return instance

    def validate_project(self, instance):
        teams = self.context['request'].user.teams
        if instance:
            instance = IncomeExpenditureProject.objects.filter(id=instance.id, teams=teams).first()
            if not instance:
                raise APIException({'message': '收支项目不存在'})
        return instance

    def validate_account(self, instance):
        teams = self.context['request'].user.teams
        if instance:
            instance = Account.objects.filter(id=instance.id, teams=teams).first()
            if not instance:
                raise APIException({'message': '结算账户不存在'})
        return instance

    def create(self, validated_data):
        teams = self.context['request'].user.teams
        creator = self.context['request'].user
        validated_data['creator'] = creator
        validated_data['creator_username'] = creator.username
        validated_data['teams'] = teams

        return super().create(validated_data)

    def save(self, **kwargs):
        validated_data = self.validated_data
        kwargs['operator_username'] = validated_data['operator'].username
        kwargs['project_type'] = validated_data['project'].type
        kwargs['account_name'] = validated_data['account'].name
        payment_target = validated_data.get('client') or validated_data.get('supplier')
        kwargs['payment_unit'] = payment_target.name if payment_target else None
        return super().save(**kwargs)
