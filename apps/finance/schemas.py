from drf_yasg.openapi import Schema, TYPE_OBJECT, TYPE_STRING


get_number_response = {
    200: Schema(type=TYPE_OBJECT, properties={
        'number': Schema(type=TYPE_STRING, title='编号')
    }),
}
