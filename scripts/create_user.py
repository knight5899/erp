from user.models import Teams, User
from sales.models import Client
from purchase.models import Supplier
from warehouse.models import Warehouse
from account.models import Account
from goods.models import Category
from django.db.transaction import atomic
import pendulum


def run(*args):
    username = input('用户名: ')
    password = input('密码: ')
    phone = input('手机号: ')
    email = input('邮箱: ')
    days = input('使用天数: ')
    expire_date = pendulum.now().add(days=float(days))

    with atomic():
        teams = Teams.objects.create(phone=phone, expire_date=expire_date)
        User.objects.create(teams=teams, username=username, password=password, phone=phone, email=email, sex='men')

        Client.objects.create(name='默认客户', teams=teams)
        Supplier.objects.create(name='默认供应商', teams=teams)
        Warehouse.objects.create(name='默认仓库', teams=teams)
        Account.objects.create(name='默认结算账户', teams=teams)
        Category.objects.create(name='默认分类', teams=teams)
